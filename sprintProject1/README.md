# Sprint-Project1

El objetivo de este spint project es crear una API para nuestro cliente Delilah Resto con el fin de facilitar la gestion de pedidos.

## Packages:

### Dependecies:
- express [v4,.7.1] [page](https://www.npmjs.com/package/express)
- express-async-errors [v3.1.1] [page](https://www.npmjs.com/package/express-async-errors)
- dotenv [v10.0.0] [page](https://www.npmjs.com/package/dotenv)
- cors [v2.8.5] [page](https://www.npmjs.com/package/cors)
- mongoose [v6.1.7] [page](https://www.npmjs.com/package/mongoose)
- jsonwebtoken [v8.5.1][page](https://www.npmjs.com/package/jsonwebtoken)
- bcryptjs [v2.4.3] [page](https://www.npmjs.com/package/bcryptjs)
- morgan [v1.10.0] [page](https://www.npmjs.com/package/morgan)
- redis [v4.0.3] [page](https://www.npmjs.com/package/redis)
- helmet [v5.0.2] [page](https://www.npmjs.com/package/helmet)

### Dev-dependencies
- nodemon [v2.0.12] [page](https://www.npmjs.com/package/nodemon)
- mocha [v9.2.0] [page](https://www.npmjs.com/package/mocha)
- chai [v4.3.6] [page](https://www.npmjs.com/package/chai)
- supertest [v6.2.2] [page](https://www.npmjs.com/package/supertest)

## Herramientas
- NodeJS [v14.17.1] [page](https://nodejs.org/es/)
- Postman [page](https://www.postman.com/downloads/)
- Swagger [page](https://swagger.io/)
- MongoDB [page](https://www.mongodb.com/try/download/community)
- Redis [page](https://redis.io/download)
- AWS [page](https://aws.amazon.com/es/console)

## Pasos

- [1] Entre a la pagina web de Amazon Web Services.

- [2]: Inicie sesion con el usuario (credenciales se encuentran en el Excel)

- [3]: Dirigirse al servicio de ec2.

- [4]: Para acceder a la instancia SprintProject3, debe agregar una regla de entrada para que permita el acceso por ssh desde su ip.

- [5]: Dirigase al grupo de seguridad de la instancia SprintProject3, y editar las reglas de entrada.
 
- [6]: Agregar una regla de entrada de TIPO SSH, y con ORIGEN: MI IP

- [7]: Luego hay que ubicarse en el mismo directorio de la llave pem para acceder a la instancia, puede ingresar este comando, o acceder mediante un archivo ejecutable.
```
ssh ec2-user@3.140.50.131 -i ./acceso-instancias
```
```
./access.sh
```

- [8]: Finalmente, el nombre de dominio web es el siguiente
```
https://primer-dominio.ml/
```

