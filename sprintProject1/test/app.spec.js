const { expect } = require("chai");
const request = require("supertest");
const app = require("../source/app");

const User = require('../source/models/users');

// Testing: Registro de Usuarios

function test() {
    describe("#app", function() {
        describe('auth "/register"', function() {
            let userId = '';

            it("Deberia devolver el status code '400' cuando no le estamos enviando credenciales", function(done) {
                request(app)
                    .post('/api/auth/register')
                    .end((err, res) => {
                        expect(res.statusCode).to.equal(400)
                        done();
                    })
            })

            it("Deberia devolver el status code '400 cuando las credenciales sean invalidas", function(done) {
                const payload = {
                    userName: '9',
                    firstName: '',
                    lastName: '',
                    email: 'user',
                    phoneNumber: '&%$&$/((%&',
                    shippingAddress: 525366,
                    password: 123456
                    
                }

                request(app)
                    .post("/api/auth/register")
                    .send(payload)
                    .end((err, res) => {
                        expect(res.statusCode).to.equal(400)
                        done();
                    })
            })

            it("Deberia devolver el status code '201', id y el token cuando las credenciales sean validas", function(done) {
                const payload = {
                    userName: 'awdqse9',
                    firstName: 'Jorge',
                    lastName: 'Gomez',
                    email: 'jgomez@mail.com',
                    phoneNumber: 627475654,
                    shippingAddress: 'belgrano 1624',
                    password: "123456789"
                }

                request(app)
                .post("/api/auth/register")
                .send(payload)
                .end((err, res) => {
                    const token = res.body.token;

                    userId = res.body?._id;

                    expect(res.statusCode).to.equal(201);
                    expect(token).to.be.a("string");
                    expect(token).to.have.lengthOf.above(0);
                    done();
                })
            })
            // Usuario de prueba eliminado
            after(async function() {
                if(userId != null) {
                    await User.findOneAndDelete(userId);
                }
            })
        })
    })
}

test();