require("dotenv").config();
require('express-async-errors');
const express = require("express");
const app = express();
const cors = require("cors");
const morgan = require('morgan');
const helmet = require('helmet');
const passport = require('passport');
const session = require('express-session');
const {isLoggedIn} = require('./middlewares/loggedIn');
require('./auth/passport-setup-google');
require('./auth/passport-setup-linkedin');
require('./auth/passport-setup-facebook');

app.use(session({
    secret: process.env.APP_SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
}));

app.use(passport.initialize());
app.use(passport.session());
// base de datos
const connectToDB = require("./db/connect");

// rutas
const users = require("./routes/users");
const products = require("./routes/products");
const orders = require("./routes/orders");
const auth = require("./routes/auth");
const paymentMethods = require("./routes/paymentMethods");

app.use(helmet());
app.use(morgan("tiny"));
app.use(session({
    secret: process.env.APP_SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
}));

const errorHandlerMiddleware = require("./middlewares/errorHandler");
const notFound = require("./middlewares/notFound");

app.use(cors());
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.get("/", (req, res) => {
	res.send("Bienvenido a Delilah Resto.");
});

app.get("/version", (req, res) => {
	res.send("Version 1.0");
});

app.get('/home', isLoggedIn, (req, res) => {
        console.log(req.user);
        return res.send({ Mensaje: `Bienvenido ${req.user.displayName}` });
    }
);

app.get('/failed', (req, res) => {
    console.log('Falla la loguearse');
    res.status(401).json({ Mensaje: 'Falla al loguearse' });
});

app.use("/api/users", users);
app.use("/api/products", products);
app.use("/api/orders", orders);
app.use("/api/auth", auth);
app.use("/api/paymentMethods", paymentMethods);

// Manejador de errores
app.use(notFound);
app.use(errorHandlerMiddleware);

const port = process.env.NODE_PORT || 3000;

const start = async () => {
    try {
        await connectToDB();
        console.log('Conectado a la base de datos');
        app.listen(port, () => {
            console.log(`Escuchando en el puerto ${port}`);
        });
    } catch (error) {
        console.log(error);
    }
};

start();

module.exports = app;
