const express = require("express");
const router = express.Router();

const {authenticateUser, authorizePermissions, authorizeUserFunctions} = require('../middlewares/authentication');

const {getAllPaymentMethods, getOnePaymentMethod, createPaymentMethod, updatePaymentMethod, deletePaymentMethod} = require('../controllers/paymentMethodController');

// USUARIO OBTIENE TODOS LOS METODOS DE PAGO
router.get("/", authenticateUser, authorizeUserFunctions, getAllPaymentMethods);

// USUARIO OBTIENE UN METODO DE PAGO POR ID
router.get("/:id", authenticateUser, authorizeUserFunctions, getOnePaymentMethod);

// ADMINSTRADOR AGREGA UN METODO DE PAGO
router.post("/", authenticateUser, authorizePermissions("admin"), createPaymentMethod);

// ADMINISTRADOR ACTUALIZA INFORMACION DE UN METODO DE PAGO POR ID
router.patch("/:id/update", authenticateUser, authorizePermissions("admin"), updatePaymentMethod);

// ADMINISTRADOR ELIMINA UN METODO DE PAGO POR ID
router.delete("/:id/delete", authenticateUser, authorizePermissions("admin"), deletePaymentMethod);

module.exports = router;