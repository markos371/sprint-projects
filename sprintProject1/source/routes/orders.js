const express = require("express");
const router = express.Router();

const {authenticateUser, authorizePermissions, authorizeUserFunctions} = require('../middlewares/authentication');

const {getAllOrders,
    getOneOrder,
    getCurrentUserOrders,
    createOrder,
    confirmOrder,
    changeStatusOrder,
    modifyOrder,
    deleteOrder} = require('../controllers/orderController');

// ADMINISTRADOR OBTIENE TODAS LAS ORDENES
router.get('/', authenticateUser, authorizePermissions('admin'), getAllOrders);

// USUARIO OBTIENE TODAS SUS ORDENES
router.get('/getOrdersUser', authenticateUser, authorizeUserFunctions, getCurrentUserOrders);

// USUARIO OBTIENE UNA DE SUS ORDENES POR ID
router.get('/:id', authenticateUser, authorizeUserFunctions, getOneOrder);

// USUARIO CREA UNA ORDEN
router.post('/', authenticateUser, authorizeUserFunctions, createOrder);

// USUARIO CONFIRMA SU ORDEN INGRESANDO ID
router.patch('/confirmOrder/:id', authenticateUser, authorizeUserFunctions, confirmOrder);

// ADMINISTRADOR ACTUALIZA ESTADO DE LA ORDEN POR ID
router.patch('/changeStatusOrder/:id', authenticateUser, authorizePermissions('admin'), changeStatusOrder);

// USUARIO MODIFICA SU ORDEN POR ID
router.patch('/modifyOrder/:id', authenticateUser, authorizeUserFunctions, modifyOrder);

// USUARIO ELIMINA SU ORDEN POR ID
router.delete('/deleteOrder/:id', authenticateUser, authorizeUserFunctions,deleteOrder);

module.exports = router;