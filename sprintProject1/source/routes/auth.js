require('../auth/passport-setup-google');
require('../auth/passport-setup-linkedin');
require('../auth/passport-setup-facebook');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const {register, login} = require('../controllers/authController');

// USUARIO SE REGISTRA (primer registrado es admin)
router.post('/register', register);

// USUARIO INICIA SESION
router.post('/login', login);

// GOOGLE REGISTER
router.get('/google',
  passport.authenticate('google', { scope: ['profile', 'email'] }),
  (req, res) => console.log('Usuario autenticado')
);

router.get('/google/callback', passport.authenticate('google', {
    failureRedirect: '/failed',
    successRedirect: '/home'
}));

// LINKEDIN REGISTER
router.get('/linkedin',
  //, { scope: ['r_basicprofile', 'r_emailaddress'] }r_emailaddress
  passport.authenticate('linkedin', { scope: ['r_liteprofile', 'r_emailaddress'], credentials: 'include' })
);

router.get('/linkedin/callback',
  passport.authenticate('linkedin', {
    failureRedirect: '/failed',
    successRedirect: '/home'
  }
  )
);

router.get('/facebook',
  // passport.authenticate('facebook', { scope: ['user_friends'] })
  // TODO. Investir scopes!
  passport.authenticate('facebook')
);

router.get('/facebook/callback',
  passport.authenticate('facebook',
    {
      failureRedirect: '/failed',
      successRedirect: '/home'
    }
  ));

router.get('/failed', (req, res) => {
    console.log('Falla al registrarse');
    res.status(401).json({ Mensaje: 'Falla al registrarse' });
});

// Usada tanto para todos los passport
router.get('/logout', (req, res) => {
    console.log('logged out');
    req.logout();
    res.status(200).redirect('/');
});

module.exports = router;