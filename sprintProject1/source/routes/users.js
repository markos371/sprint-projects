const express = require("express");
const router = express.Router();

const {authenticateUser, authorizePermissions, authorizeUserFunctions} = require('../middlewares/authentication');
const {getAllUsers, getSingleUser, showCurrentUser, suspendAndEnableUser, addAddresses} = require('../controllers/userController');

// ADMINISTRADOR OBTIENE TODOS LOS USUARIOS REGISTRADOS
router.get('/', authenticateUser, authorizePermissions('admin'), getAllUsers);

// USUARIO OBTIENE CIERTA INFORMACION (id, name, role, estadod de suspension)
router.get('/showMe', authenticateUser, showCurrentUser);

// USUARIO OBTIENE TODA SU INFORMACION SI INGRESA SU ID
router.get('/:id', authenticateUser, authorizeUserFunctions, getSingleUser);

// ADMINISTRADOR MODIFICA ESTADO DE SUSPENSION A SU OPUESTO
router.patch('/suspend/:id', authenticateUser, authorizePermissions('admin'), suspendAndEnableUser);

// USUARIO AGREGA DIRECCION DE ENVIO A SUS PEDIDOS
router.patch('/:id/addShippingAddresses', authenticateUser, authorizeUserFunctions, addAddresses)

module.exports = router;