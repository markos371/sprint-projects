const express = require("express");
const router = express.Router();

const {authenticateUser, authorizePermissions, authorizeUserFunctions} = require('../middlewares/authentication');

const {getAllProducts, getOneProduct, createProduct, updateProduct, updateProductPrice, deleteProduct} = require('../controllers/productController');

// USUARIO OBTIENE TODOS LOS PRODUCTOS
router.get('/', authenticateUser, authorizeUserFunctions, getAllProducts);

// USUARIO OBTIENE UN PRODUCTO POR ID
router.get('/:id', authenticateUser, authorizeUserFunctions, getOneProduct);

// ADMINISTRADOR AGREGA UN NUEVO PRODUCTO
router.post('/', authenticateUser, authorizePermissions('admin'), createProduct);

// ADMINISTRADOR ACTUALIZA LA INFORMACION DE UN PRODUCTO POR ID
router.patch('/:id/update', authenticateUser, authorizePermissions('admin'), updateProduct);

// ADMINISTRADOR ACTUALIZA EL PRECIO DE UN PRODUCTO POR ID
router.patch('/:id/updatePrice', authenticateUser, authorizePermissions('admin'), updateProductPrice);

// ADMINISTRADOR ELIMINA UN PRODUCTO POR ID
router.delete('/:id/delete', authenticateUser, authorizePermissions('admin'), deleteProduct);

module.exports = router;