const {UnauthenticatedError, UnauthorizedError} = require('../errors');
const jwt = require('jsonwebtoken');

// VERIFICACION: CREDENCIALES VALIDAS
const authenticateUser = async (req, res, next) => {
    const authHeader = req.headers.authorization;

    if(!authHeader || !authHeader.startsWith('Bearer')) {
        throw new UnauthenticatedError('Autenticacion invalida');
    }
    const token = authHeader.split(' ')[1];
    if (!token) {
        throw new UnauthenticatedError('Autenticacion invalida');
      }

    try {
        const {name, userId, role, suspended} = jwt.verify(token, process.env.JWT_SECRET)
        req.user = {name, userId, role, suspended};
        next();
    } catch(error) {
        throw new UnauthenticatedError('Autenticacion invalida');
    }
};

// VERIFICACION: ES ADMINISTRADOR
const authorizePermissions = (...roles) => {
    return (req, res, next) => {
        if(!roles.includes(req.user.role)) {
            throw new UnauthorizedError('No esta autorizado a acceder a esta ruta');
        }
        next();
    }
};

// VERIFICACION: AUTORIZACION DE USUARIO PARA ACCEDER A UNA RUTA (estado de suspension)
const authorizeUserFunctions = (req, res, next) => {
    if(req.user.suspended) {
        throw new UnauthorizedError('Usuario suspendido, no esta autorizado a acceder a esta ruta');    
    }
    next();
};

module.exports = {
    authenticateUser,
    authorizePermissions,
    authorizeUserFunctions
};