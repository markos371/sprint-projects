// DETERMINA EL ERROR
const errorHandlerMiddleware = (err, req, res, next) => {
    let customError = {
        statusCode: err.statusCode || 500,
        msg: err.message || 'Intente nuevamente mas tarde'
    }

    if(err.name === 'ValidationError') {
        customError.msg = Object.values(err.errors)
        .map(item => item.message)
        .join(' - ');
        customError.statusCode = 400;
    }

    if(err.code && err.code === 11000) {
        customError.msg = `Valor duplicado ingresado para ${Object.keys(
            err.keyValue
        )}, ingrese otro valor por favor`;
        customError.statusCode = 400;
    }

    if(err.name === 'CastError') {
        customError.msg = `No hay ningun item con id: ${err.value}`;
        customError.statusCode = 404;
    }

    return res.status(customError.statusCode).json({msg: customError.msg});
};

module.exports = errorHandlerMiddleware;