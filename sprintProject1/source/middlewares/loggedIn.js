const isLoggedIn = (req, res, next) => {
    if (req.isAuthenticated()) {
        next();
    } else {
        throw new UnauthenticatedError('Autenticacion invalida');
    }
};

module.exports = {isLoggedIn}