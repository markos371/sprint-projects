const notFound = (req, res) => res.status(404).send('Ruta inexistente');

module.exports = notFound;