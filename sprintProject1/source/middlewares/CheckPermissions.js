const {UnauthorizedError} = require('../errors');

// VERIFICACION: RECURSO PERTENECE A USUARIO O ADMIN
const CheckPermissions = (requestUser, resourceUserId) => {
    if(requestUser.role === 'admin') 
        return
    if(requestUser.userId === resourceUserId.toString()) 
        return 
    throw new UnauthorizedError('No esta autorizado para acceder a esta ruta');
};

module.exports = {CheckPermissions};