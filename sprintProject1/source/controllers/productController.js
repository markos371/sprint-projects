const Product = require('../models/products');
const {NotFoundError} = require('../errors');
const client = require('../db/redis');
const { confirmOrder } = require('./orderController');

// limpia la cache (llave:products)
const cleanCache = () => {
    client.del("products");
}

// Obtiene todos los productos
const getAllProducts = async (req, res) => {
    try {
        const reply = await client.get('products');
      
        if(reply) {
            return res.status(200).json({products: JSON.parse(reply), count: reply.length});
        }

        const products = await Product.find({});

        await client.set("products", JSON.stringify(products), {EX: 10});

        return res.status(200).json({products, count: products.length});
    } catch (err) {
        console.log(err);
    }
};

// OBTENER UN PRODUCTO POR ID
const getOneProduct = async (req, res) => {
    const {id: productId} = req.params;

    const product = await Product.findOne({_id: productId});

    if(!product) {
        throw new NotFoundError(`No hay producto con id: ${productId}`);
    }

    res.status(200).json({product});
};

// Crear un producto
const createProduct = async (req, res) => {
    const product = await Product.create(req.body);

    res.status(201).json({msg: `Producto creado`, product});
};

// ACTUALIZAR UN PRODUCTO
const updateProduct = async (req, res) => {
    const {id: productId} = req.params;

    const product = await Product.findOneAndUpdate({_id: productId}, req.body, {
        new: true,
        runValidators: true
    });

    if(!product) {
        throw new NotFoundError(`No hay productos con id: ${productId}`);
    }

    res.status(200).json({msg: `Producto actualizado`, product});
};

// ACTUALIZAR PRECIO DE UN PRODUCTO
const updateProductPrice = async (req, res) => {
    const {id: productId} = req.params;

    const product = await Product.findOne({_id: productId});

    if(!product) {
        throw new NotFoundError(`No hay producto con id: ${productId}`);
    }

    product.price = req.body.price;

    await product.save();

    cleanCache();

    res.status(200).json({msg: `Precio del producto actualizado`, product});
};

// ELIMINAR UN PRODUCTO
const deleteProduct = async (req, res) => {
    const {id: productId} = req.params;

    const product = await Product.findOne({_id: productId});

    if(!product) {
        throw new NotFoundError(`No hay productos con id: ${productId}`);
    }

    await product.remove();

    res.status(200).json({msg: 'Producto removido'});
};

module.exports = {
    getAllProducts,
    getOneProduct,
    createProduct,
    updateProduct,
    updateProductPrice,
    deleteProduct
};
