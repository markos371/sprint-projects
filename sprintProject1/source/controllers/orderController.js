const Order = require('../models/orders');
const Product = require('../models/products');
const User = require('../models/users');
const PaymentMethod = require('../models/paymentMethods');

const {BadRequestError, NotFoundError} = require('../errors');

const {CheckPermissions} = require('../middlewares/CheckPermissions');


// Obtiene el listado de productos y calcula el precio total de la orden
const getDetailAndTotal = async (items) => {
    let detail = [];
    let total = 0;
    
    for(let item of items) {
        const oneProductItems = await Product.findOne({_id: item.product});
        if(!oneProductItems) {
            throw new NotFoundError(`No hay producto con id: ${item.product}`);
        }

        const {name, price, _id} = oneProductItems;
        const singleOrderItem = {
            name,
            amount: item.amount,
            price,
            product: _id
        }

        detail.push(singleOrderItem);
        total += item.amount * price;
    }

    return {detail, total}
};

// OBTENER TODOS LAS ORDENES
const getAllOrders = async (req, res) => {
    const orders = await Order.find({});
    res.status(200).json({orders, count: orders.length});
};

// OBTENER UNA ORDEN POR ID
const getOneOrder = async (req, res) => {
    const {id: orderId} = req.params;
    const order = await Order.findOne({_id: orderId});
    if(!order) {
        throw new NotFoundError(`No hay orden con id: ${orderId}`)
    }

    CheckPermissions(req.user, order.user);
    res.status(200).json({order});
};

// OBTENER TODAS LAS ORDENES DE UN USUARIO
const getCurrentUserOrders = async (req, res) => {
    const orders = await Order.find({user: req.user.userId});
    res.status(200).json({orders, count: orders.length});
};

// CREAR UNA ORDEN
const createOrder = async (req, res) => {
    const {items, paymentMethodId} = req.body;

    if(!items || items.length < 1) {
        throw new BadRequestError('No hay productos en la orden');
    }
    if(!paymentMethodId) {
        throw new BadRequestError('No ingreso metodo de pago');
    }

    const dbPaymentMethod = await PaymentMethod.findOne({_id: paymentMethodId});

    if(!dbPaymentMethod) {
        throw new NotFoundError(`No hay metodo de pago con id: ${paymentMethodId}`);
    }

    const {detail, total} = await getDetailAndTotal(items);

    const order = await Order.create({
        detail,
        total,
        paymentMethod: dbPaymentMethod._id,
        user: req.user.userId
    });

    const user = await User.findOne({_id: req.user.userId});

    if(user) {
        return res.status(201).json({order, shippingAddresses: user.shippingAddresses});
    } 
    return res.status(201).json({msg: `Orden creada`, order})
};

// CONFIRMAR UNA ORDEN
const confirmOrder = async (req, res) => {
    const {id: orderId} = req.params;
    const order = await Order.findOne({_id: orderId});
    
    if(!order) {
        throw new NotFoundError(`No hay orden con id: ${orderId}`);
    }
    CheckPermissions(req.user, order.user);

    order.status = "Confirmado";

    await order.save();

    res.status(200).json({msg: `Orden confirmada`});
};

// CAMBIAR ESTADO DE UNA ORDEN VALIDA
const changeStatusOrder = async (req, res) => {
    const {id: orderId} = req.params;
    const order = await Order.findOne({_id: orderId});

    if(!order) {
        throw new NotFoundError(`No hay orden con id: ${orderId}`);
    }

    const {status} = req.body;

    order.status = status;

    await order.save();

    res.status(200).json({msg: `Estado de la orden modificado`});
};

// MODIFICAR UNA ORDEN
const modifyOrder = async (req, res) => {
    const {id: orderId} = req.params;
    const order = await Order.findOne({_id: orderId});

    if(!order) {
        throw new NotFoundError(`No hay orden con id: ${orderId}`);
    }
    CheckPermissions(req.user, order.user);

    if(order.status !== "Pendiente") {
        throw new BadRequestError(`Error, su orden ya esta cerrada`);
    }

    const {items, paymentMethodId} = req.body;

    if(!items || items.length < 1) {
        throw new BadRequestError('No hay productos en la orden');
    }
    if(!paymentMethodId) {
        throw new BadRequestError('No ingreso metodo de pago');
    }

    const dbPaymentMethod = await PaymentMethod.findOne({_id: paymentMethodId});

    if(!dbPaymentMethod) {
        throw new NotFoundError(`No hay metodo de pago con id: ${paymentMethodId}`);
    }

    const {detail, total} = await getDetailAndTotal(items);

    order.detail = detail;
    order.total = total;
    order.paymentMethod = dbPaymentMethod._id;
    order.user = req.user.userId;

    await order.save();

    res.status(200).json({msg: `Orden modificada`, order});
};

// USUARIO ELIMINA UNA DE SUS ORDENES 
const deleteOrder = async (req, res) => {
    const {id: orderId} = req.params;
    const order = await Order.findOne({_id: orderId});

    if(!order) {
        throw new NotFoundError(`No hay orden con id: ${orderId}`);
    }
    
    CheckPermissions(req.user, order.user);

    if(order.status !== "Pendiente") {
        throw new BadRequestError('Error, su orden ya esta cerrada');
    }
    await order.remove();
    res.status(200).json({msg: `Orden eliminada`});
};

// FALTA IMPORTARLO DE LA RUTA DE ORDENES, ACA ESTA TODA LA LOGICA LISTA
module.exports = {
    getAllOrders,
    getOneOrder,
    getCurrentUserOrders,
    createOrder,
    confirmOrder,
    changeStatusOrder,
    modifyOrder,
    deleteOrder
};