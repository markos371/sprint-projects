const User =require('../models/users');
const {BadRequestError, UnauthenticatedError} = require('../errors');

// REGISTRO DE USUARIO NUEVO
const register = async (req, res) => {
    const {userName, firstName, lastName, email, phoneNumber, shippingAddresses, password} = req.body;
    
    const emailDuplicated = await User.findOne({email});

    if(emailDuplicated) {
        throw new BadRequestError("Este email ya esta registrado");
    }

    // el primer usuario registrado es el admin
    const isFirstAccount = (await User.countDocuments({})) === 0;
    const role = isFirstAccount ? 'admin' : 'user';
    const suspended = false;

    const user = await User.create({userName, firstName, lastName, email, phoneNumber, shippingAddresses, password, role, suspended})
    const token = user.createJWT();
    res.status(201).json({message: `Usuario creado`, _id: user._id, token});
};

// INICIO DE SESION USUARIO
const login = async (req, res) => {
    const {email, password} = req.body;
    console.log(email, password);

    if(!email || !password) {
        throw new BadRequestError('Ingrese usuario y contrasena');
    }

    const user = await User.findOne({email});

    if(!user) {
        throw new UnauthenticatedError('Credenciales invalidas');
    }

    const isCorrectPassword = await user.comparePassword(password);

    if(!isCorrectPassword) {
        throw new UnauthenticatedError('Credenciales invalidas');
    }

    const token = user.createJWT();
    
    res.status(200).json({msg: `Se inicio sesion`, token});
};

module.exports = {
    register,
    login
};
