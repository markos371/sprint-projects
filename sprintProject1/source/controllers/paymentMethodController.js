const PaymentMethod = require('../models/paymentMethods');
const {NotFoundError} = require('../errors');

// Obtiene el listado de metodos de pago
const getAllPaymentMethods = async (req, res) => {
    const paymentMethods = await PaymentMethod.find({});

    res.status(200).json({paymentMethods, count: paymentMethods.length});
};

// OBTENER UNA ORDEN POR ID
const getOnePaymentMethod = async (req, res) => {
    const {id: paymentMethodId} = req.params;

    const paymentMethod = await PaymentMethod.findOne({_id: paymentMethodId});

    if(!paymentMethod) {
        throw new NotFoundError(`No hay metodo de pago con id: ${paymentMethodId}`);
    }

    res.status(200).json({paymentMethod});
};

// CREAR METODO DE PAGO
const createPaymentMethod = async (req, res) => {
    const paymentMethod = await PaymentMethod.create(req.body);

    res.status(201).json({msg: `Metodo de pago creado`, paymentMethod});
};


// ACTUALIZAR METODO DE PAGO
const updatePaymentMethod = async (req, res) => {
    const {id: paymentMethodId} = req.params;

    const paymentMethod = await PaymentMethod.findOneAndUpdate({_id: paymentMethodId}, req.body, {
        new: true,
        runValidators: true
    });

    if(!paymentMethod) {
        throw new NotFoundError(`No hay metodo de pago con id: ${paymentMethodId}`);
    }

    res.status(200).json({msg: `Metodo de pago actualizado`, paymentMethod});
};

// ELIMINAR METODO DE PAGO
const deletePaymentMethod = async (req, res) => {
    const {id: paymentMethodId} = req.params;

    const paymentMethod = await PaymentMethod.findOne({_id: paymentMethodId});

    if(!paymentMethod) {
        throw new NotFoundError(`No hay metodo de pago con id: ${paymentMethodId}`);
    }

    await paymentMethod.remove();
    res.status(200).json({msg: `Medio de pago eliminado`});
};

module.exports = {
    getAllPaymentMethods,
    getOnePaymentMethod,
    createPaymentMethod,
    updatePaymentMethod,
    deletePaymentMethod
};