const User = require('../models/users');
const {NotFoundError} = require('../errors');
const {CheckPermissions} = require('../middlewares/CheckPermissions');

// OBTENER TODOS LOS USUARIOS REGISTRADOS
const getAllUsers = async (req, res) => {
    const users = await User.find({role: 'user'})
    res.status(200).json({users});
};

// Obtener un usuario por id
const getSingleUser = async (req, res) => {
    const {id: userId} = req.params;

    const user = await User.findOne({_id: userId})
    if(!user) {
        throw new NotFoundError(`No hay usuario con id: ${userId}`);
    }
    CheckPermissions(req.user, user._id);
    res.status(200).json({user});
};

// obtener cierta informacion de un usuario actual
const showCurrentUser = async (req, res) => {
    res.status(200).json({user: req.user});
};

// Modifica estado de suspension a su opuesto
const suspendAndEnableUser = async (req, res) => {
    const {id: userId} = req.params;

    const user = await User.findOne({_id: userId});

    if(!user || userId === req.user.userId) {
        throw new NotFoundError(`No hay usuario con id: ${userId}`);
    }

    user.suspended = !(user.suspended);

    await user.save();

    res.status(200).json({user});
};

// Agrega una direccion a la lista de direcciones de envio
const addAddresses = async (req, res) => {
    const {id: userId} = req.user.userId;
    const addresses = req.body.addresses;

    const user = await User.findOne({id: userId})

    user.shippingAddresses = user.shippingAddresses.concat(addresses);

    await user.save();

    res.status(200).json({msg: `Direcciones de envio agregadas`});
};

module.exports = {
    getAllUsers,
    getSingleUser,
    showCurrentUser,
    suspendAndEnableUser,
    addAddresses
};