const mongoose = require('mongoose');

const SingleOrderIteMSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    amount: {
        type: Number, 
        required: true
    },
    price : {
        type: Number, 
        required: true
    },
    product: {
        type: mongoose.Schema.ObjectId,
        ref: 'Product',
        required: true
    }
});

const OrderSchema = new mongoose.Schema({
    detail: [SingleOrderIteMSchema],
    total: {
        type: Number,
        required: true,
    },
    status: {
        type: String,
        enum: ['Pendiente', 'Confirmado', 'En preparacion', 'Enviado', 'Entregado'],
        default: 'Pendiente'
    },
    paymentMethod: {
        type: mongoose.Schema.ObjectId,
        ref: 'PaymentMethod',
        required: true
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    }
});

module.exports = mongoose.model('Order', OrderSchema);