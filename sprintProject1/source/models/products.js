const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Nombre requerido']
    },
    description: {
        type: String,
        required: [true, 'Descripcion del producto requerido'],
        maxlength: [1000, 'La descripcion no puede superar los 1000 caracteres']
    },
    price: {
        type: Number,
        required: [true, 'Precio del producto requerido'],
        default: 0
    }
})

module.exports = mongoose.model('Product', ProductSchema);