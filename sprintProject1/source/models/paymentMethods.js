const mongoose = require('mongoose');

const PaymentMethodSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Medio de pago requerido']
    }
});

module.exports = mongoose.model('PaymentMethod', PaymentMethodSchema);