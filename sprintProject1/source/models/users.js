const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const UserSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: [true, 'Nombre de usuario requerido'],
        minlength: 2,
        maxlength: 32,
    },
    firstName: {
        type: String,
        required: [true, 'Nombre requerido'],
        maxlength: 40
    },
    lastName: {
        type: String,
        required: [true, 'Apellido requerido'],
        maxlength: 40
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'Email requerido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Ingrese un email valido']
    },
    phoneNumber: {
        type: Number,
        required: [true, 'Numero de telefono requerido']
    },
    shippingAddresses: {
        type: [String],
        require: [true, `Direccion de envio requerida`]
    },
    password: {
        type: String,
        required: [true, 'Contrasena requerida'],
        minlength: 8,
        maxlength: 128
    },
    role: {
        type: String,
        enum: ['admin', 'user'],
        default: 'user',
    },
    suspended: {
        type: Boolean,
        default: false
    },
})

// se encripta la contrasena
UserSchema.pre('save', async function() {
    if(!this.isModified('password')) return;
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
});

// comparacion contrasena
UserSchema.methods.comparePassword = async function(password) {
    const match = await bcrypt.compare(password, this.password);
    return match
};

// se genera un token
UserSchema.methods.createJWT = function() {
    return jwt.sign({userId: this._id, name: this.userName, role: this.role, suspended: this.suspended}, 
                    process.env.JWT_SECRET, {expiresIn: process.env.JWT_LIFETIME});
}

module.exports = mongoose.model('User', UserSchema);