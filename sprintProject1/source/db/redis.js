const redis = require('redis');

const client = redis.createClient({
    host: process.env.REDIS_URI,
    port: 6379
});

/*const client = redis.createClient(6379, process.env.REDIS_URI, {
        no_ready_check: true
     });
*/

client.on('error', (err) => console.log('Redis Client Error', err));

client.on('connect', function() {
    console.log('Redis Client connected');
});

module.exports = client;
