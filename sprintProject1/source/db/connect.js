const mongoose = require("mongoose");

/*const connectToDB = (url) => {
    return mongoose.connect(url)
};*/

const connectToDB = () => {
    const URI = process.env.MONGO_URI;
    console.log(URI);
    mongoose.connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }),
    (err, res) => {
        if(!err) {
            console.log(`Conexion exitosa`);
        }else {
            console.log(`Error de conexion`);
        }
    };
};

module.exports = connectToDB;
